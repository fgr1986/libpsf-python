#!/bin/bash
set -e -x

#############################################################################
#############################################################################
###
###
###         REQUIRES:  $PYBIN
###
###         THIS WILL BUILD A WHEEL FOR PYTHON SYSTEM FOUND AT $PYBIN
###
###
#############################################################################
#############################################################################

if [ -z ${PYBIN+x} ]; then
    echo 'you have not supplied $PYBIN'
    exit 1
fi


# in wheel root:
###########################################
cd /io/libpsf
cp /io/README.md .

# update tools and install reqs
"${PYBIN}/pip" install --upgrade pip
"${PYBIN}/pip" install -r /io/dev-requirements.txt

# start building
"${PYBIN}/python" setup.py build_ext
"${PYBIN}/pip" wheel --no-deps -w /io/wheelhouse/ .

# use python 3 to audit the wheel
echo "*****************installing auditwheel**********************"
/opt/python/cp37-cp37m/bin/pip install auditwheel
/opt/python/cp37-cp37m/bin/python -m auditwheel repair /io/wheelhouse/*libpsf*.whl -w /io/wheelhouse/

# if some other wheels get in the wheelhouse, remove them
( rm /io/wheelhouse/*numpy*.whl )

# install the wheel and test
"${PYBIN}/pip" install -f /io/wheelhouse --no-index libpsf
#"${PYBIN}/nosetests" libpsf
teststr="$( ${PYBIN}/python -c 'import libpsf; result=True if "PSFDataSet" in dir(libpsf) else False; print(result)' )"
echo "teststr: $teststr"
if test $teststr == "False"; then
    echo "failed test"
    return 1
else
    echo "passed test"
fi