#!/usr/bin/env bash

WHEEL_DIR="$1"

for WHEEL in "$WHEEL_DIR"/libpsf*manylinux*.whl; do
  twine upload --verbose "$WHEEL"
done